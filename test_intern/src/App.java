
public class App {
    public String sum(String numA, String numB) {
        StringBuilder result = new StringBuilder();

        int maxLen = Math.max(numA.length(), numB.length());
        int lengthA = numA.length();
        int lengthB = numB.length();
        int soDu = 0;

        numA = swapIndex(numA);
        numB = swapIndex(numB);
        System.out.println(numA);
        System.out.println(numB);

        if (maxLen > lengthA) {
            for (int i = 0; i < lengthA; i++) {
                numA += "0";
            }
        }
        if (maxLen > lengthB) {
            for (int i = 0; i < lengthB; i++) {
                numB += "0";
            }
        }

        for (int i = 0; i < maxLen; i++) {
            int tong = 0;
            tong = numA.charAt(i) - '0' + numB.charAt(i) - '0' + soDu;
            System.out.println(tong);
            result.append(tong % 10);
            soDu = tong / 10;
        }
        if (soDu == 1) {
            result.append(1);
        }

        return swapIndex(result.toString());
    }

    public String swapIndex(String num) {
        StringBuilder result = new StringBuilder();
        for (int i = num.length() - 1; i >= 0; i--) {
            result.append(num.charAt(i));
        }
        return result.toString();
    }


    public static void main(String[] args) {
        App abab = new App();
        String a = abab.sum("123323", "3212312");
        System.out.println("Result: " + a);
    }
}
